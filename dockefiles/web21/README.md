# docker
## @edt ASIX-M05 Curs 2021-2022

# Servidor web Apache

Aquesta pràctica consisteix en generar un servidor Web Apache amb una pàgina web bàsica de benvinguda.

Requeriments:

  * Identificar el nom del paquet del apache en la distribució GNU/Linux a utilitzar.
  * Crear una pàgina web de benvinguda.
  * Identificar el directori de publicació web del servidor apache.
  * Identificar l’executable o l’ordre per engegar el servidor web en foreground.
  * Important, identificar l’opció que l’engega en foreground.

```
$ pwd
./docker/dockefiles/web21

$ ls
Dockerfile  index.html
```

```
$ cat Dockerfile
# servidor web basat en debian:latest usant apache 
FROM debian:latest
LABEL author="@edt ASIX 2022"
LABEL curs="docker /AWS"
RUN apt-get update && apt-get install -y procps iproute2 iputils-ping nmap tree vim apache2
COPY index.html /var/www/html/
WORKDIR /tmp
EXPOSE 80
CMD  apachectl -k start -X 
```

```
$ cat index.html 
<html>
    <head>
        <title>ASIX-M05 Dockers</title>
    </head>
    <body>
        <h1>web 1hisix</h1>
        <h2>practica M05 docker</h2>
        hola soc la pagina web
	d'exemple de ASIX-M05 dockers
    </body>
</html>
```

```
$ docker build -t prova-web21 .

$ docker run --rm -p 80:80 -d prova-web21
029c5bb921126be4c8aa5d205cdb5d23b06d2ecab586ccf14a2bdf07f8128fd0

$ docker ps
CONTAINER ID   IMAGE         COMMAND                  CREATED         STATUS         PORTS                               NAMES
029c5bb92112   prova-web21   "/bin/sh -c 'apachec…"   2 seconds ago   Up 2 seconds   0.0.0.0:80->80/tcp, :::80->80/tcp   stupefied_montalcini

$ docker stop stupefied_montalcini 
stupefied_montalcini

$ docker rmi prova-web21:latest 
```




