# docker
## @edt ASIX-M05 Curs 2021-2022

# Servidor Samba

Aquesta activitat consisteix en crear un servidor Samba-3 Stand Alone que comparteixi recursos de disc (File Shares) on s’hi poden connectar tant clients GNU/Linux com clients Windows.

Requeriments:

  * Identificar el nom del paquet del servei Samba, de fet dels serveis nmb i smb.
  * Compartir els recursos:
    *  /var/lib/samba/public
    * /var/lib/samba/privat
  * Posar-hi xixa per poder comprovar que hi ha accés als recursos.
  * Identificar l’executable o l’ordre per engegar els serveis.
  * Important, identificar l’opció que engega en foreground almenys un dels serveis l’últim de tots que ha de quedar actiu.

```
$ pwd
/var/tmp/m05/docker/dockefiles/samba21

$ ls
Dockerfile  smb.conf  startup.sh
```

```
$ cat Dockerfile 
FROM debian:latest
LABEL version="1.0"
LABEL author="@edt ASIX-M06 Curs 2021-2022"
LABEL subject="SAMBA Server"
#ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get -y install procps samba
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
```

```
$ cat startup.sh 
#! /bin/bash
# @edt ASIX M06 2019-2020
# startup.sh
# -------------------------------------

# Creació de xixa per als shares
mkdir /var/lib/samba/public
chmod 777 /var/lib/samba/public
cp /opt/docker/* /var/lib/samba/public/.

mkdir /var/lib/samba/privat
#chmod 777 /var/lib/samba/privat
cp /opt/docker/*.md /var/lib/samba/privat/.

# Configuració samba
cp /opt/docker/smb.conf /etc/samba/smb.conf

# Creació usuaris unix/samba
useradd -m -s /bin/bash lila
useradd -m -s /bin/bash roc
useradd -m -s /bin/bash patipla
useradd -m -s /bin/bash pla
echo -e "lila\nlila" | smbpasswd -a lila
echo -e "roc\nroc" | smbpasswd -a roc
echo -e "patipla\npatipla" | smbpasswd -a patipla
echo -e "pla\npla" | smbpasswd -a pla

# Activar els serveis
/usr/sbin/smbd && echo "smb Ok"
/usr/sbin/nmbd -F && echo "nmb  Ok"
```

```
$ cat smb.conf 
[global]
        workgroup = MYGROUP
        server string = Samba Server Version %v
        log file = /var/log/samba/log.%m
        max log size = 50
        security = user
        passdb backend = tdbsam
        load printers = yes
        cups options = raw
[homes]
        comment = Home Directories
        browseable = no
        writable = yes
;       valid users = %S
;       valid users = MYDOMAIN\%S
[printers]
        comment = All Printers
        path = /var/spool/samba
        browseable = no
        guest ok = no
        writable = no
        printable = yes
[documentation]
        comment = Documentació doc del container
        path = /usr/share/doc
        public = yes
        browseable = yes
        writable = no
        printable = no
        guest ok = yes
[manpages]
        comment = Documentació man  del container
        path = /usr/share/man
        public = yes
        browseable = yes
        writable = no
        printable = no
        guest ok = yes
[public]
        comment = Share de contingut public
        path = /var/lib/samba/public
        public = yes
        browseable = yes
        writable = yes
        printable = no
        guest ok = yes
[privat]
        comment = Share d'accés privat
        path = /var/lib/samba/privat
        public = no
        browseable = no
        writable = yes
        printable = no
        guest ok = yes
```

```
$ docker build -t prova-samba21 .
$ docker run --rm --name samba21 -p 139:139 -p 445:445  -d prova-samba21
c8d105da2ebdf09589139ae28fdd8235e02c7bc36c4667464cb94ff9c1adacb1

$ docker ps
CONTAINER ID   IMAGE           COMMAND                  CREATED         STATUS         PORTS                                                                          NAMES
c8d105da2ebd   prova-samba21   "/bin/sh -c /opt/doc…"   4 seconds ago   Up 2 seconds   0.0.0.0:139->139/tcp, :::139->139/tcp, 0.0.0.0:445->445/tcp, :::445->445/tcp   samba21
```

```
$ smbclient //172.17.0.2/public
Enter SAMBA\ecanet's password: 
Anonymous login successful
Try "help" to get a list of possible commands.
smb: \> ls
  .                                   D        0  Thu Jul  7 20:03:19 2022
  ..                                  D        0  Thu Jul  7 20:03:20 2022
  Dockerfile                          N      340  Thu Jul  7 20:03:19 2022
  startup.sh                          N      807  Thu Jul  7 20:03:19 2022
  smb.conf                            N     1399  Thu Jul  7 20:03:19 2022

		51343840 blocks of size 1024. 21652616 blocks available
smb: \> quit
```

```
$ smbclient //localhost/public
Enter SAMBA\ecanet's password: 
Anonymous login successful
Try "help" to get a list of possible commands.
smb: \> ls
  .                                   D        0  Thu Jul  7 20:07:34 2022
  ..                                  D        0  Thu Jul  7 20:07:34 2022
  Dockerfile                          N      340  Thu Jul  7 20:07:34 2022
  startup.sh                          N      807  Thu Jul  7 20:07:34 2022
  smb.conf                            N     1399  Thu Jul  7 20:07:34 2022

		51343840 blocks of size 1024. 21684328 blocks available
smb: \> quit
```

```
$ docker stop samba21 
samba21

$ docker rmi prova-samba21:latest 
```



