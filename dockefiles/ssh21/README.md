# docker
## @edt ASIX-M05 Curs 2021-2022

# Servidor SSH

En aquesta pràctica cal construir una imatge amb Dockerfile d’un servidor SSH. Per poder-lo provar el servidor incorporarà un conjunt d’usuaris pre creats que permetin verificar el seu funcionament.

El servidor també incorporarà les ordres client així es podrà utilitzar per verificar internament o desplegar-ne copies interactives que actuin com a clients.
 
Requeriments:

  * Identificar el nom dels paquets del servidor i el client SSH. 
  * Fer un script que crea automàticament els usuaris prefixats.
  * Identificar l’executable o l’ordre per engegar el servidor en foreground.
  * Important, identificar l’opció que l’engega en foreground.
  * Segons la distribució utilitzada primer cal generar les claus de host (Fedora) però en Debian no cal.

```
$ pwd
./docker/dockefiles/ssh21

$ ls
Dockerfile  useradd.sh
```

```
$ cat Dockerfile 
# SSH server exemple detach amb Debian
FROM debian
LABEL subject="SSH server"
LABEL author="@edt ASIX M05"
RUN apt-get update && apt-get -y install openssh-client openssh-server
COPY useradd.sh /tmp
RUN bash /tmp/useradd.sh
WORKDIR /tmp
CMD /etc/init.d/ssh start -D
EXPOSE 22
```

```
$ cat useradd.sh 
#! /bin/bash
# Creació d'usuaris prefixats per verificar accés SSH

for user in pere marta anna pau jordi julia
do
    useradd -m $user
    echo $user:$user | chpasswd 
done
```

```
$ docker build -t prova-ssh21 .

$ docker run --rm --name ssh21 -p 22:22 -d prova-ssh21
42b17f9ff2ec5283152af743e1d5e288e8de8fd1df4f9c2e5c9b57fbed0b6a8a

$ docker ps
CONTAINER ID   IMAGE         COMMAND                  CREATED         STATUS         PORTS                               NAMES
42b17f9ff2ec   prova-ssh21   "/bin/sh -c '/etc/in…"   2 seconds ago   Up 2 seconds   0.0.0.0:22->22/tcp, :::22->22/tcp   ssh21

$ docker stop ssh21 
Ssh21

$ docker rmi prova-ssh21:latest 
```





