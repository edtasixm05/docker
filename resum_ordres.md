# Docker commands


## Sessió 1

**Gneral**

```
docker version
docker --version

docker run hello-world
docker ps [-a]
docker container ls [-a]
docker inspect <container>

docker images
docker history <image>
docker pull <image>

docker rm <container>
docker rmi <image>
```

**Run**

```
docker run <options> -it image <command>

docker run -it fedora
docker run -it fedora:27 /bin/bash
docker run -it fedora date
docker run -it fedora:27 cat /etc/os-relaase
```

**Commit / Build / Tag**

```
docker commit <container> <new-image-name>
docker build -t <new-image-name> .

docker tag <image> <new-image-name>
docker tag mylinux edtasixm05/mylinux:latest
docker tag mylinux edtasixm05/mylinux:v2

docker login
docker push edtasixm05/mylinux:v2
docker push edtasixm05/mylinux
docker logout

docker search edtasixm05
```

```
FROM fedora
LABEL author="@edt 2022"
LABEL curs="docker"
RUN yum -y install procps nmap tree vim iproute iputils
WORKDIR /tmp
CMD /bin/bash
```

### Sessió 2

```
FROM debian
LABEL author="@edt 2022"
RUN apt-get update && apt-get -y install nmap tree vim procps iproute2 iputils-ping apache2
WORKDIR /tmp
EXPOSE 80
CMD apachectl -k start -X
```

**Detach**

```
docker build -t edtasixm05/myweb:latest .
docker run --rm -d edtasixm05/myweb:latest 
docker ps
```

```
docker exec -it <container> /bin/bash
docker exec -it <container> <ordre>
```

**Ports**

```
# Ports: ip-adress-host:port-host:port-container

docker run --rm -p 80:80 -d edtasixm05/myweb
docker run --rm -p 0.0.0.0:80:80 edtasixm05/myweb

docker run --rm -p 9000:80 -d edtasixm05/myweb
docker run --rm -p 127.0.0.1:9000:80 -d edtasixm05/myweb

docker run --rm -P -d edtasixm05/myweb

docker ps
docker port <container>
```

**Bind Mounts**

```
# editar el contingut de /tmp/web/index.html i observar els canvis a la web

docker run --rm -v /tmp/web/index.hmtl:/var/www/html/indx.html -d edtasixm05/myweb
docker run --rm -v /tmp/web:/var/html/www -d edtasixm05/myweb 
```

```
# Corregir exercicis d'alumnes 

docker run --rm -v /tmp/lliuraments:/lliuraments -it debian
docker run --rm -v /tmp/python/lliuraments:/exercicis -it python:3 /bin/bash
```

**Volumes**

```
docker volume ls
docker volume create volum1
docker volume create volum2

docker volume ls
docker volume inspect volum1
sudo tree /var/lib/docker/volumes

docker volume rm volum1
docker volume prune

docker volume create mydata
```

```
docker run --rm -v mydata:/opt -it debian

sudo tree /var/lib/docker/volumes
```






