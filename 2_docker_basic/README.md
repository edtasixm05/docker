# docker
## @edt ASIX-M05 Curs 2021-2022


## Docker bàsic


#### Publicació de ports

Exemples de propagació (publicació) de ports.


#### Mounts (bind mounts & Volumes)

Exemples d'utilització de mounts: bind mounts i volumes

 * Scripts de bash per corregir

 * Programes python per corregir



#### Entrypoint / CMD


  * **entry-cmd-net** Exemples fets per usar en les explicacions dels conceptes
    *entrypoint*, *cmd* i *network*.

   * **cmd** imatge amb un cmd *date* per observar el funcionament de cmd.

   * **entry** imatge amb un entrypoint *date* per observar el funcionament de entry.

   * **entryopc** Afegit un exemple amb un script *opcions.sh* d'entrypoint per fer dia,
     o sysinfo o calendari.

   * **entrybd**  Afegit un escript d'exemple de entrypoint que simula una imatge amb 
     un gestor de base de dades amb les opcions initdb, destroy, dump, start.


#### Network

  * **ssh** Imatge servidor/client ssh amb algunes utilitats de xarxa.


#### Variables

Exemples d'utilització de variables del shell per passar als containers.


#### Secrets

Exemples d'utilització de secrets.


