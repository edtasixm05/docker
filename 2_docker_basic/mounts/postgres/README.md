 docker
## @edt ASIX-M05 Curs 2021-2022


### Usar postgres (la nostra versió) i una base de dades persistent

Anem a veure un exemple més complert d’utilització d’un volum com a mecanisme de persistència de dades d’una base de dades. Usarem un container Postres amb la base de dades d’exemple de l’escola Training. 

Primerament observarem que si no usem volums tots els canvis a la base de dades es perden si s’elimina el container. Si es crea un nou container aquest torna a tenir totes les dades originals.

Després crearem un volum on contenir les dades de la base de dades, associant-lo al directori de dades de Postgres. D’aquesta manera veurem que si fem actualitzacions a les dades encara que el container s’elimini les dades actualitzades perduren. En crear de nou un container usant el volum els canvis es mantenen.

  * Engegar un container postgres amb la base de dades training. S’utilitza una imatge ja preparada que incorpora postgres i training.

```
$ docker run --rm --name training  -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training  -d edtasixm05/getstarted:postgres 
3d7a38d668ba5dd3856291ce785943e58db93e8520327e51633eef93de0bc2d6

$ docker ps
CONTAINER ID   IMAGE                            COMMAND                  CREATED         STATUS         PORTS      NAMES
3d7a38d668ba   edtasixm05/getstarted:postgres   "docker-entrypoint.s…"   2 seconds ago   Up 2 seconds   5432/tcp   training
```

  * Verificar les taules i llistar algun registre. En aquest exemple ho fem generant un altre container i executant l’interpret psql que connecta amb el servidor postgres (el servidor ha de ser l’adreça IP indicada). El password és passwd.

```
$ docker run -it --rm  postgres psql -h 172.17.0.2 -U postgres -d training
Password for user postgres: passwd
psql (14.3 (Debian 14.3-1.pgdg110+1))
Type "help" for help.

training=# \d
           List of relations
 Schema |   Name    | Type  |  Owner   
--------+-----------+-------+----------
 public | clientes  | table | postgres
 public | oficinas  | table | postgres
 public | pedidos   | table | postgres
 public | productos | table | postgres
 public | repventas | table | postgres
(5 rows)

training=# select * from oficinas;
 oficina |   ciudad    | region | dir | objetivo  |  ventas   
---------+-------------+--------+-----+-----------+-----------
      22 | Denver      | Oeste  | 108 | 300000.00 | 186042.00
      11 | New York    | Este   | 106 | 575000.00 | 692637.00
      12 | Chicago     | Este   | 104 | 800000.00 | 735042.00
      13 | Atlanta     | Este   | 105 | 350000.00 | 367911.00
      21 | Los Angeles | Oeste  | 108 | 725000.00 | 835915.00
(5 rows)

training=# \q
```

  * Una altra manera de verificar les dades del servidor Postgres és entrant-hi directament amb docker exec i realitzar una sessió de l’interpret SQL psql, o directament exeutar-lo. En ser local al servidor no cal password.

```
$ docker exec -it training  psql -d training -U postgres
psql (14.3 (Debian 14.3-1.pgdg110+1))
Type "help" for help.

training=# \d
           List of relations
 Schema |   Name    | Type  |  Owner   
--------+-----------+-------+----------
 public | clientes  | table | postgres
 public | oficinas  | table | postgres
 public | pedidos   | table | postgres
 public | productos | table | postgres
 public | repventas | table | postgres
(5 rows)

training=# select * from repventas;
 num_empl |    nombre     | edad | oficina_rep |   titulo   |  contrato  | director |   cuota   |  ventas   
----------+---------------+------+-------------+------------+------------+----------+-----------+-----------
      105 | Bill Adams    |   37 |          13 | Rep Ventas | 1988-02-12 |      104 | 350000.00 | 367911.00
      109 | Mary Jones    |   31 |          11 | Rep Ventas | 1989-10-12 |      106 | 300000.00 | 392725.00
      102 | Sue Smith     |   48 |          21 | Rep Ventas | 1986-12-10 |      108 | 350000.00 | 474050.00
      106 | Sam Clark     |   52 |          11 | VP Ventas  | 1988-06-14 |          | 275000.00 | 299912.00
      104 | Bob Smith     |   33 |          12 | Dir Ventas | 1987-05-19 |      106 | 200000.00 | 142594.00
      101 | Dan Roberts   |   45 |          12 | Rep Ventas | 1986-10-20 |      104 | 300000.00 | 305673.00
      110 | Tom Snyder    |   41 |             | Rep Ventas | 1990-01-13 |      101 |           |  75985.00
      108 | Larry Fitch   |   62 |          21 | Dir Ventas | 1989-10-12 |      106 | 350000.00 | 361865.00
      103 | Paul Cruz     |   29 |          12 | Rep Ventas | 1987-03-01 |      104 | 275000.00 | 286775.00
      107 | Nancy Angelli |   49 |          22 | Rep Ventas | 1988-11-14 |      108 | 300000.00 | 186042.00
(10 rows)

training=# \q
```

  * Anem a fer modificacions a l’engròs. Ens carreguem la taula oficinas.

```
$ docker exec -it training  psql -d training -U postgres
psql (14.3 (Debian 14.3-1.pgdg110+1))
Type "help" for help.

training=# drop table oficinas;
DROP TABLE

training=# select * from oficinas;
ERROR:  relation "oficinas" does not exist
LINE 1: select * from oficinas;
                      ^
training=# \q
```

  * Si tanquem el servidor i l’engeguem de nou veiem que la taula oficinas torna a ser-hi, no hi ha persistència de les dades als canvis.

```
$ docker stop training 
Training

$ docker run --rm --name training  -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training  -d edtasixm05/getstarted:postgres 
a66b08656988cf6ec297eafa07c00039c289e42c044a7442a62536529a24e977

$ docker exec -it training  psql -d training -U postgres \d
psql: warning: extra command-line argument "d" ignored
psql (14.3 (Debian 14.3-1.pgdg110+1))
Type "help" for help.

training=# \d
           List of relations
 Schema |   Name    | Type  |  Owner   
--------+-----------+-------+----------
 public | clientes  | table | postgres
 public | oficinas  | table | postgres
 public | pedidos   | table | postgres
 public | productos | table | postgres
 public | repventas | table | postgres
(5 rows)

training=# select * from oficinas;
 oficina |   ciudad    | region | dir | objetivo  |  ventas   
---------+-------------+--------+-----+-----------+-----------
      22 | Denver      | Oeste  | 108 | 300000.00 | 186042.00
      11 | New York    | Este   | 106 | 575000.00 | 692637.00
      12 | Chicago     | Este   | 104 | 800000.00 | 735042.00
      13 | Atlanta     | Este   | 105 | 350000.00 | 367911.00
      21 | Los Angeles | Oeste  | 108 | 725000.00 | 835915.00
(5 rows)

training=# \q
```

```
$ docker stop training 
training
```

**Persistència**

Anem a fer que les dades de Postgres de la base de dades Training no resideixin en el container sinó en un volum anomenat postgres-data.  En la imatge utilitzada Postgres desa les dades de les bases de dades en el directori **/var/lib/postgresql/data**, però el directoris de dades depèn de la distribució de GNU/Linux utilitzada.

  * Primerament creem el volum.

  * Després engeguem Postgres associant el volum al directori de dades de Postgres segons el sistema operatiu que s’estigui utilitzant en el container.

```
$ docker run --rm --name training  -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training  -v postgres-data:/var/lib/postgresql/data -d edtasixm05/getstarted:postgres 
1e07bd7282c5e0fa93863fad7c485da4bbef5289480c023d7d5e0c2fd6fe455a

$ docker ps
CONTAINER ID   IMAGE                            COMMAND                  CREATED         STATUS         PORTS      NAMES
1e07bd7282c5   edtasixm05/getstarted:postgres   "docker-entrypoint.s…"   3 seconds ago   Up 2 seconds   5432/tcp   training
```

  * Podem observar que en crear-se el container el populate de dades s’ha fet dins del volum postgres-data.

```
$ sudo tree /var/lib/docker/volumes/postgres-data/ | head
/var/lib/docker/volumes/postgres-data/
└── _data
    ├── base
    │   ├── 1
    │   │   ├── 112
    │   │   ├── 113
    │   │   ├── 1247
    │   │   ├── 1247_fsm
    │   │   ├── 1247_vm
    │   │   ├── 1249
```

  * Ara ens carreguem la taula oficinas.

```
$ docker exec -it training  psql -d training -U postgres 
psql (14.3 (Debian 14.3-1.pgdg110+1))
Type "help" for help.

training=# drop table oficinas;
DROP TABLE

training=# \d
           List of relations
 Schema |   Name    | Type  |  Owner   
--------+-----------+-------+----------
 public | clientes  | table | postgres
 public | pedidos   | table | postgres
 public | productos | table | postgres
 public | repventas | table | postgres
(4 rows)

training=# \q
```

  * Aturem el servei postgres de manera que el container s’eliminarà. L’engeguem de nou en un nou container usant el volum de dades i observem que ara no existeix la taula oficinas.

```
$ docker stop training 
training

$ docker run --rm --name training  -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training  -v postgres-data:/var/lib/postgresql/data -d edtasixm05/getstarted:postgres 
f022424e73c1ecd9474cae287c39d544de0187fa3860dfd630e2b375e3d1d014

$ docker exec -it training  psql -d training -U postgres 
psql (14.3 (Debian 14.3-1.pgdg110+1))
Type "help" for help.

training=# \d
           List of relations
 Schema |   Name    | Type  |  Owner   
--------+-----------+-------+----------
 public | clientes  | table | postgres
 public | pedidos   | table | postgres
 public | productos | table | postgres
 public | repventas | table | postgres
(4 rows)

training=# \q
```

  * Aturem el servidor postgres.

```
$ docker stop training 
training
```


### Usar la imatge postgres original amb populate i persistència de dades

En aquesta pràctica veurem com usar la imatge original de Postgres que proporcionen els propis creadors. Conté un mecanisme per poder-la inicialitzar amb un contingut concret, fer el **populate** de dades, usant scripts de linux o de SQL. I un cop creada la base de dades com proporcionar persistència a les dades usant un volum.

Els creadors de la imatge de Postgres proporcionen un mecanisme automàtic per crear un container amb dades ja incorpprades. En crear el container tot allò que hi hagi en el directori **/docker-entrypoint-initdb.d** s’executarà per ordre alfabètic, siguin scripts de shell o de SQL. El truc consisteix en preparar en un directori les dades i scripts d’incialització i fer un **bind mount** d’aquest directori amb el directori /docker-entrypoint-initdb.d. D’aquesta manera en crear-se el container els executa i omple la Base de Dades.

Si a més a més volem tenir persistència de dades podem crear un **volum** i associar-lo al directori de dades de Postgres. Aquest directori depèn de la distribució, però en aquest cas és el directori **/var/lib/postgresql/data**. 

  * Crear el volum de dades si no existeix.

```
$ docker volume create postgres-data
Postgres-data

$ docker volume ls
DRIVER    VOLUME NAME
local     postgres-data
```

  * Crear un directori de desenvolupament on tenir els fitxers script i dades amb els que volem fer el populate de la base de dades. En el nostre cas utilitzarem un directori ja preparat dels apunts de GitLab de docker i ens situarem en aquest directori com a directori actiu per engegar el servei.

  * Si no el tenim ja descarregat descarregar del Git el material del curs i anar al directori pertinent. Atenció! Els directoris poden canviar de lloc.

```
$ pwd
/tmp

$ git clone https://www.gitlab.com/edtasixm05/docker.git
Cloning into 'docker'...
warning: redirecting to https://gitlab.com/edtasixm05/docker.git/
remote: Enumerating objects: 675, done.
remote: Counting objects: 100% (497/497), done.
remote: Compressing objects: 100% (481/481), done.
remote: Total 675 (delta 214), reused 0 (delta 0), pack-reused 178
Receiving objects: 100% (675/675), 3.65 MiB | 5.59 MiB/s, done.
Resolving deltas: 100% (290/290), done.

$ cd docker/mounts/postgres/training/

$ pwd
/tmp/docker/mounts/postgres/training

$ ls
clientes.dat  instalar_postgres.txt  oficinas.sql  pedidos.sql    productos.sql  repventas.sql
clientes.sql  oficinas.dat           pedidos.dat   productos.dat  repventas.dat
```

  * Engegar el servei postgres indicant el bind mount del directori de inicialització i el volum de dades on desar la base de dades training. També s’indica el nom de la base de dades i el password a usar.
    * **–name training** el nom del container
    * **-e POSTGRES_PASSWORD=passwd** defineix el nom del password per connectar com usuari postgres.
    * **-e POSTGRES_DB=training** defineix el nom de la base de dades a la que connectar o crear.
    * **-v $(pwd):/docker-entrypoint-initdb.d** indica que cal fer el bind mount del directori actiu (on hi ha els scripts i les dades d’inicialització) al directori preparat per Postgres per contenir la inicialització.
    * **-v postgres-data:/var/lib/postgresql/data** indica el volum de dades per tenir la persistència de les dades de postgres. Es munta sobre el directori on postgres desa totes les bases de dades.
    * **-d postgres** imatge postgres oficial a engegar en detach.

```
$ docker run --rm --name training  -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training -v $(pwd):/docker-entrypoint-initdb.d  -v postgres-data:/var/lib/postgresql/data -d postgres
cac7f74b3cd6a4e210614ffd215077f7d33907c1fdf93116caa67b5521024417

$ docker ps
CONTAINER ID   IMAGE      COMMAND                  CREATED         STATUS         PORTS      NAMES
cac7f74b3cd6   postgres   "docker-entrypoint.s…"   3 seconds ago   Up 2 seconds   5432/tcp   training
```

```
$ docker exec -it training  psql -d training -U postgres 
psql (14.3 (Debian 14.3-1.pgdg110+1))
Type "help" for help.

training=# \d
           List of relations
 Schema |   Name    | Type  |  Owner   
--------+-----------+-------+----------
 public | clientes  | table | postgres
 public | pedidos   | table | postgres
 public | productos | table | postgres
 public | repventas | table | postgres
(4 rows)

training=# \q
```

  * Aturar el container i eliminar els volums si no els usem

```
$ docker stop training 
training

$ docker volume prune
```

### Exemples de SQL injectat usant volumes

Anem a usr el container de postgres amb la base de dades training per practicar exemples de SQL injectat, especialment exemples nocius. Per fer-ho usarem un directory local on tenim preparats scripts de python que ataquen la base de dades Postgres Training. Els incorporarem al container usant un bind mount del directori dels exercicis dins el container.

  * Primerament ens siturem en el directori dels apunts on hi ha els exercicis d’exemple. Si no ho hem fet abans podem descarregar els apunts del GitLab. Atnció que la ubicació dels directoris pot canviar.

```
$ cd /tmp/

$ git clone https://www.gitlab.com/edtasixm05/docker.git
Cloning into 'docker'...
warning: redirecting to https://gitlab.com/edtasixm05/docker.git/
remote: Enumerating objects: 640, done.
remote: Counting objects: 100% (462/462), done.
remote: Compressing objects: 100% (448/448), done.
remote: Total 640 (delta 209), reused 0 (delta 0), pack-reused 178
Receiving objects: 100% (640/640), 3.64 MiB | 6.48 MiB/s, done.
Resolving deltas: 100% (285/285), done.

$ cd mounts/postgres/sql-injection/

$ pwd
/tmp/docker/mounts/postgres/sql-injection

$ ls
12-popen-sql.py  13-popen-sql-injectat.py  13-sql-injectat.py  14-popen-sql-multi.py  README.md
```

  * Engegar el servei postgres en la xarxa per defecte.

```
$ docker run --rm --name training  -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training  -d edtasixm05/getstarted:postgres
d57c366ad7b0075579e22555e1b520b06870da3756c8ca23f91b22f70caf003b
```

  * Engegar un client psql interactu que comunica amb el servei postgres anterior, és una imatge ja preparada edtasixm05/postgres21:client-psql-py amb Python i el client psql de Postgres (el password de postgres és passwd). Munta el directori sql-injection amb els exemples de python a provar al directori /tmp per poder practicar.

```
$ docker run  --rm -v $(pwd):/tmp -it  edtasixm05/postgres21:client-psql-py

/ # python /tmp/12-popen-sql.py 
Password for user postgres: 
2111,JCP Inc.,103,50000.00
2102,First Corp.,101,65000.00
2103,Acme Mfg.,105,50000.00
2123,Carter & Sons,102,40000.00
2107,Ace International,110,35000.00
2115,Smithson Corp.,101,20000.00
2101,Jones Mfg.,106,65000.00
2112,Zetacorp,108,50000.00
2121,QMA Assoc.,103,45000.00
2114,Orion Corp,102,20000.00
2124,Peter Brothers,107,40000.00
2108,Holm & Landis,109,55000.00
2117,J.P. Sinclair,106,35000.00
2122,Three-Way Lines,105,30000.00
2120,Rico Enterprises,102,50000.00
2106,Fred Lewis Corp.,102,65000.00
2119,Solomon Inc.,109,25000.00
2118,Midwest Systems,108,60000.00
2113,Ian & Schmidt,104,20000.00
2109,Chen Associates,103,25000.00
2105,AAA Investments,101,45000.00

/ # python /tmp/13-sql-injectat.py  "select * from oficinas;"
Password for user postgres: 
22,Denver,Oeste,108,300000.00,186042.00
11,New York,Este,106,575000.00,692637.00
12,Chicago,Este,104,800000.00,735042.00
13,Atlanta,Este,105,350000.00,367911.00
21,Los Angeles,Oeste,108,725000.00,835915.00

/ # python /tmp/14-popen-sql-multi.py -c 2103 -c 2107
Password for user postgres: 
2103,Acme Mfg.,105,50000.00
2107,Ace International,110,35000.00
```

  * Exemples '**nocius**' de SQL Injectat

```
/ # python /tmp/13-sql-injectat.py "select * from oficinas; drop table oficinas;"
Password for user postgres:


/ # python /tmp/13-sql-injectat.py  "select * from oficinas;"
Password for user postgres: 
ERROR:  relation "oficinas" does not exist
LINE 1: select * from oficinas;


/ # python /tmp/14-popen-sql-multi.py -c 2103 -c "2107;drop table pedidos;"
Password for user postgres: 
2103,Acme Mfg.,105,50000.00
2107,Ace International,110,35000.00

/ # python /tmp/13-sql-injectat.py  "select * from pedidos;"
Password for user postgres: 
ERROR:  relation "pedidos" does not exist
LINE 1: select * from pedidos;


/ # python /tmp/14-popen-sql-multi.py -c 2103 -c "2107;drop database training;"
Password for user postgres: 
2103,Acme Mfg.,105,50000.00
2107,Ace International,110,35000.00
##nota: no elimina la base de dades
```





