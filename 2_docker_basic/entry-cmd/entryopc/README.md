# docker
## @edt ASIX-M05 Curs 2021-2022


### Entrypoint amb script d'opcions


Aquest és un exemple on la imatge està hardcoded amb un etnrypoint corresponent a un script que simplement permet:
 * dia: mostrar la data
 * sysinfo: mostrar informació del /etc/os-release
 * calendari: mostrar el calendari

Com que és un entrypoint en principi només permet l'execució
'aquest script. L'escrip si no rep arguments mostra la data,
si rep l'argument *date*, *sysinfo* o *cal* fa l'acció corresponent.

```
$ docker build -t testentry .

$ docker run --rm -it testentry
Tue Jun 28 17:40:56 UTC 2022

$ docker run --rm -it testentry patata
Tue Jun 28 17:43:20 UTC 2022
 
$ docker run --rm -it testentry dia
Tue Jun 28 17:43:29 UTC 2022
 
$ docker run --rm -it testentry sysinfo
PRETTY_NAME="Debian GNU/Linux 11 (bullseye)"
NAME="Debian GNU/Linux"
VERSION_ID="11"
VERSION="11 (bullseye)"
VERSION_CODENAME=bullseye
ID=debian
HOME_URL="https://www.debian.org/"
SUPPORT_URL="https://www.debian.org/support"
BUG_REPORT_URL="https://bugs.debian.org/"

$ docker run --rm -it testentry calendari
     June 2022        
Su Mo Tu We Th Fr Sa  
          1  2  3  4  
 5  6  7  8  9 10 11  
12 13 14 15 16 17 18  
19 20 21 22 23 24 25  
26 27 28 29 30        

```




