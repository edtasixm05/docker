# docker
## @edt ASIX-M05 Curs 2021-2022

# Ordres Generals

En aquest apartat es descriuran les ordres generals i més bàsiques necessàries per aprendre a treballar amb l’entorn de comandes amb docker.  

Són concptes claus a adquirir:

 * Identificar la versió de l’Engine i del Client.
 * Identificar el concepte d’imatge. Tractar imatges: llistar, identificar, eliminar, descarregar, capes que la formen  i observar-ne la descipció en JSON.
 * Identificar el concepte de container. Tractar containers: executar, llistar eliminar, identificar el nom de host i el nom de container.

```
docker version
docker --version

docker run hello-world
docker ps [-a]
docker container ls [-a]
docker inspect <container>

docker images
docker history <image>
docker pull <image>

docker rm <container>
docker rmi <image>
```

Docker és una arquitectura client/servidor que es comunica a través d’un socket unix. El client són les ordres de consola que realitzem i l’Engine és el servei que les executa.

```
$ docker --version
Docker version 20.10.6, build 370c289
 
$ docker version
Client: Docker Engine - Community
 Version:           20.10.6
 API version:       1.41
 Go version:        go1.13.15
 Git commit:        370c289
 Built:             Fri Apr  9 22:47:32 2021
 OS/Arch:           linux/amd64
 Context:           default
 Experimental:      true

Server: Docker Engine - Community
 Engine:
  Version:          20.10.6
  API version:      1.41 (minimum version 1.12)
  Go version:       go1.13.15
  Git commit:       8728dd2
  Built:            Fri Apr  9 22:45:12 2021
  OS/Arch:          linux/amd64
  Experimental:     false
 containerd:
  Version:          1.4.4
  GitCommit:        05f951a3781f4f2c1911b05e61c160e9c30eaa8e
 runc:
  Version:          1.0.0-rc93
  GitCommit:        12644e614e25b05da6fd08a38ffa0cfe1903fdec
 docker-init:
  Version:          0.19.0
  GitCommit:        de40ad0


$ ls -la /var/run/docker.sock 
srw-rw----. 1 root docker 0 Jul  4 18:45 /var/run/docker.sock
```

---

## Hello-world

Basant-nos en el famós exemple hello world anem a observar que docker funciona i els seus components principals: imatges i containers. 

Una imatge és un com un DVD o CD una estructura de disc de només lectura que està formada per diverses capes (com una imatge RAW d’una màquina virtual)

Un container és una imatge més una capa superior Read/Write i els processos que s’hi executen (com una màquina virtual en execució però NO és una màquina virtual).

```
$ docker run hello-world
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
2db29710123e: Pull complete 
Digest: sha256:13e367d31ae85359f42d637adf6da428f76d75dc9afeb3c21faea0d976f5c651
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
```


Podem observar que en l’execució anterior no ha trobat la imatge hello-world i l’ha descarregada del repositori públic DockerHub. Un cop descarregada ha creat un container basat en la imatge i l’ha executat. L’execució mostra informació de docker  i un cop acabada el container ha finalitzat i s’ha aturat (però encara existeix).

Anem a observar:

 * Entre les imatges descarregades ara hi ha hello-world. Omb l’ordre docker images es poden observar les dades següents de la imatge descarregada:

   * **REPOSITORY** Nom identificatiu de la imatge, conjuntament amb el TAG.
   * **TAG** Identificador de la imatge per indicar per exemple la versió.
   * **IMAGE_ID** Identificador numèric universal únic de la imatge (en realitat és més gran del que es mostra)
   * **CREATED** data de creació de la imatge
   * **SIZE** Mida de la imatge.

 * No hi ha cap container executant-se però si es llisten tots s’observa que que se n’ha executat un fa un moment basat en hello-world.

 * Els container si no es creen amb un nom concret reben un nom aleatori (sovint divertit) que genera docker, en l’exemple silly_williamson.

 * Amb l’ordre docker ps [-a]  podem observar les següents dades d’un container:

   * **CONTAINER-ID** Identificador numèric únic del container. En realitat és motl més llarg, aquí es mostra només una part. Quan un container no té nom de host (FQDN) utilitza el seu ID com a hostname.
   * **IMAGE** Imatge en la que s’ha basat el container, de la que deriva.
   * **COMMAND** Ordre que s’executa en engegar el container. Aquesta ordre correspón al procés amb el PID numèro 1 del container. Aquest estarà viu (engegant) mentre ho estigui el procés amb el PID número 1.
   * **CREATED** Identificar del temps que fa que s’ha creat el container.
   * **STATUS** Estat del container.
   * **PORTS** Ports usats pel conatiner.
   * **NAMES** Nom identificatiu del container (no del host, no és el hostname). Si no se n’ha indicat un d’explícit docker en genera un de propi.

 * Els container es poden esborra amb docker rm <container>. Un ruc usual per saber els container aturats que tenim és prémer docker rm [tab][tab]. Atenció: acumular container aturats (stopped) acostuma a ser un mal negoci i genera problemes quan els assignem noms perquè no n’hi poden haver dos amb el mateix nom. Per tant és molt recomanable fer constantment neteja dels container que no fan falta.

 * Donada una imatge es poden observar les capes que la formen, és una superposició de imatges una sobre l’altra amb docker history <image>.

 * Les imatges locals es poden eliminar amb docker rmi <image>. Generalment el tabulador ens ajudarà a completar el nom de la imatge, però podem tenir dificultats si la imatge té més d’un tag o més d’un nom diferent. També es poden eliminar per el seu ID.

```
$ docker images
REPOSITORY                    TAG              IMAGE ID       CREATED         SIZE
hello-world                   latest           feb5d9fea6a5   9 months ago    13.3kB
```

```
$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES

$ docker ps  -la
CONTAINER ID   IMAGE         COMMAND    CREATED         STATUS                     PORTS     NAMES
a2ae294423fa   hello-world   "/hello"   3 minutes ago   Exited (0) 3 minutes ago             silly_williamson

$ docker container ls
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES

$ docker container ls -a
CONTAINER ID   IMAGE                    COMMAND       CREATED         STATUS                     PORTS     NAMES
a2ae294423fa   hello-world              "/hello"      4 minutes ago   Exited (0) 4 minutes ago             silly_williamson
```

```
$ docker rm silly_williamson 
silly_williamson

$ docker ps -a
CONTAINER ID   IMAGE                    COMMAND       CREATED        STATUS                    PORTS     NAMES
```

```
$ docker images
REPOSITORY                    TAG              IMAGE ID       CREATED         SIZE
hello-world                   latest           feb5d9fea6a5   9 months ago    13.3kB

$ docker history hello-world:latest 
IMAGE          CREATED        CREATED BY                                      SIZE      COMMENT
feb5d9fea6a5   9 months ago   /bin/sh -c #(nop)  CMD ["/hello"]               0B        
<missing>      9 months ago   /bin/sh -c #(nop) COPY file:50563a97010fd7ce…   13.3kB    

$ docker rmi hello-world:latest 
Untagged: hello-world:latest
Untagged: hello-world@sha256:13e367d31ae85359f42d637adf6da428f76d75dc9afeb3c21faea0d976f5c651
Deleted: sha256:feb5d9fea6a5e9606aa995e879d862b825965ba48de054caab5ef356dc6b3412
Deleted: sha256:e07ee1baac5fae6a26f30cabfe54a36d3402f96afda318fe0a96cec4ca393359
```

**Atenció**: Un dels problemes usuals i crucials de docker és entendre la diferència entre container i imatge!



## Tornem-hi amb Hello-world

Anem a repetir la feina feta partint de que s’ha eliminat el container i la imatge de hello-world. Anem ara a fer el següent:

  * Descarregar la imatge hello world del repositori públic de DockerHub directament usant l’ordre docker pull <image>. Amb aquesta ordre es baixa del Cloud la imatge i es queda una còpia local.

  * Executar de nou un container basat en aquesta imatge. Cal observar que ara ja no mostra el missatge de que no ha trobat localment la imatge i l’ha de descarregar tal i com deia en el primer exemple que hem fet.

  * Observar que el nom de la imatge és altre cop aleatori generat per docker (en l’exemple thirsty_dirac).

 * Observar la descripció complerta d’un container, tota la seva definició. L’ordre docker inspect <container> o docker container inspect <container> permet veure tot el JSON que descriu un objecte conatiner. Observeu per exemple com el ID és molt més llarg del que mostra per defecte.

```
$ docker images
REPOSITORY                    TAG              IMAGE ID       CREATED         SIZE

$ docker pull hello-world
Using default tag: latest
latest: Pulling from library/hello-world
2db29710123e: Pull complete 
Digest: sha256:13e367d31ae85359f42d637adf6da428f76d75dc9afeb3c21faea0d976f5c651
Status: Downloaded newer image for hello-world:latest
docker.io/library/hello-world:latest

$ docker images
REPOSITORY                    TAG              IMAGE ID       CREATED         SIZE
hello-world                   latest           feb5d9fea6a5   9 months ago    13.3kB
```

```
$ docker run hello-world

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
```

```
$ docker ps -a
CONTAINER ID   IMAGE                    COMMAND       CREATED          STATUS                      PORTS     NAMES
0fb5e3724120   hello-world              "/hello"      19 seconds ago   Exited (0) 18 seconds ago             thirsty_dirac
```

```
$ docker inspect thirsty_dirac | less
[
    {
        "Id": "0fb5e37241208c08587c36dc8c1511545078216dd32584c3ed6b43a084d13316",
        "Created": "2022-07-04T18:37:42.887715048Z",
        "Path": "/hello",
        "Args": [],
        "State": {
            "Status": "exited",
            "Running": false,
            "Paused": false,
…
```


## Generar un container basat en GNU/Linux

En aquest apartat anem a veure com generar i treballar en containers basats en GNU/Linux i aprofitarem per aprendre els següents conceptes:

  * Generar container interactius amb docker run basats en imatges estàndard de diferents distribucions linux. L’ordre que genera un container basat en una imatge és:

```
docker run [opcions] -it imatge ordre
```

  * Els elements de l'ordre són:

    * **-it** Si es vol treballar interacticament en el container per exemple en un shell cal indicar: -i per interactive i -t per tty (terminal).

    * **nomimatge** El nom de la imatge en la que es basa el container, per exemple fedora:27 o debian que és el mateix que dir debian:latest.
Observeu que el nom de la imatge d’aquests exemples es composa de dues parts, el nom i el tag. Si no s’indica el tag docker utilitza per defecte el tag latest. Així per exemple dir fedora a seques és el mateix que dir fedora:latest. En canvi si es diu fedora:32 s’està indicant que es vol la imatge de fedora versió 32.
  
    * **ordre** L’últim component és l’ordre a excutar per iniciar el container. Si es vol treballar en una sessió interactiva usualment és un shell com per exemple /bin/bash.

  * Observeu de l’exemple següent que inicia un fedora:27 en una sessió interactiva en un shell bash.

  * Com que no s’ha indicat cap nom al container el nom de host del container és el seu ID (en nom FQDN).

  * No confondre amb el nom de container que es pot observar fent docker ps en una altra sessió. Ara com que el container està encara engegat des d’una altra sessió es poden observar les seves característiques.

  * Observeu que com que estem en una sessió interactiva ara el prompt mostra l’usuari i el nom de host. L’usuari és root i el nom de host el ID del container. Per defecte sempre som root dins d’un container.

  * Observeu també que el container no disposa de moltes de les ordres usuals del sistema operatiu (en aquest exemple no hi ha les ordres hostname, ip a, ps ax, etc). Això és perquè els container tenen una mida reduïda i s’han eliminat molts paquets innecessaris. Si es vol poder usar aquestes ordres caldrà instal·lar-les.

  * Fixeu-vos en la mida de la imatge de fedora:27 descarregada usant docker images, en aquest cas 236 MBytes. Tenim una consola en un sistema GNU/Linux fedora:27 per 236MB en lloc dels Gigas i Gigas que acostumem a destinar a les màquines virtuals!

  * I què fem en el container? Doncs allò que es vulgui. És un ‘mini’ sistema operatiu i es pot usar per practicar sistemes, aprendre a treballar en la línia de comandes, desplegar serveis, etc. En finalitzar tancar la sessió amb exit.

  * Observar que el container s’ha aturat però no eliminat. Apareix si fem docker ps -a o docker container ls -a.

**Atenció**: un dels problemes usuals en debutants és no tenir clar quan estem dins el container en una sessió interactiva i quan estem fora en el shell del nostre propi sistema!

```
$ docker run -it fedora:27 /bin/bash

[root@63a70bec5459 /]# hostname
bash: hostname: command not found

[root@63a70bec5459 /]# ip a
bash: ip: command not found

[root@63a70bec5459 /]# ps ax
bash: ps: command not found

[root@63a70bec5459 /]# id       
uid=0(root) gid=0(root) groups=0(root)

[root@63a70bec5459 /]# cat /etc/os-release 
NAME=Fedora
VERSION="27 (Twenty Seven)"
ID=fedora
VERSION_ID=27
PRETTY_NAME="Fedora 27 (Twenty Seven)"
ANSI_COLOR="0;34"
CPE_NAME="cpe:/o:fedoraproject:fedora:27"
HOME_URL="https://fedoraproject.org/"
SUPPORT_URL="https://fedoraproject.org/wiki/Communicating_and_getting_help"
BUG_REPORT_URL="https://bugzilla.redhat.com/"
REDHAT_BUGZILLA_PRODUCT="Fedora"
REDHAT_BUGZILLA_PRODUCT_VERSION=27
REDHAT_SUPPORT_PRODUCT="Fedora"
REDHAT_SUPPORT_PRODUCT_VERSION=27
PRIVACY_POLICY_URL="https://fedoraproject.org/wiki/Legal:PrivacyPolicy"
```

```
$ docker ps
CONTAINER ID   IMAGE       COMMAND       CREATED         STATUS         PORTS     NAMES
63a70bec5459   fedora:27   "/bin/bash"   4 minutes ago   Up 3 minutes             wizardly_shtern
```

```
$ docker images fedora:27
REPOSITORY   TAG       IMAGE ID       CREATED       SIZE
fedora       27        f89698585456   3 years ago   236MB
```

```
[root@63a70bec5459 /]# exit
exit

$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```

## Executar container com a ordres de GNU/Linux

En aquest apartat veurem que a part de treballar en sessions interactives en containers també podem usar-los per executar ordres ‘desateses’. Es crea el container basat en una imatge GNU/Linux per executar una ordre concreta, no una sessió en un shell.

 * Crear un container basat en debian i executar l’ordre id o l’ordre cat /etc/os-release. Observei que ara l’opció -it ja no és necessària però es pot continuar usant.

 * Practicar també en un container basat en una imatge alpine. Observeu la increïble mida de les imatges alpine! Només 5.56M, amb això tenim un GNU/Linux!

 * Proveu també un fedora i l’ordre date.

 * Finalment tronem a fer neteja de containers! És important per ser endreçats i evitar problemes posteriors.  O potser hem d’aprendre a fer que seliminin sols en plegar…

```
$ docker run -it debian id
uid=0(root) gid=0(root) groups=0(root)

$ docker run -it debian cat /etc/os-release
PRETTY_NAME="Debian GNU/Linux 11 (bullseye)"
NAME="Debian GNU/Linux"
VERSION_ID="11"
VERSION="11 (bullseye)"
VERSION_CODENAME=bullseye
ID=debian
HOME_URL="https://www.debian.org/"
SUPPORT_URL="https://www.debian.org/support"
BUG_REPORT_URL="https://bugs.debian.org/"

$ docker run  debian id
uid=0(root) gid=0(root) groups=0(root)

$ docker run  debian cat /etc/os-release
PRETTY_NAME="Debian GNU/Linux 11 (bullseye)"
NAME="Debian GNU/Linux"
VERSION_ID="11"
VERSION="11 (bullseye)"
VERSION_CODENAME=bullseye
ID=debian
HOME_URL="https://www.debian.org/"
SUPPORT_URL="https://www.debian.org/support"
BUG_REPORT_URL="https://bugs.debian.org/"
```

```
$ docker run -it alpine cat /etc/os-release
NAME="Alpine Linux"
ID=alpine
VERSION_ID=3.16.0
PRETTY_NAME="Alpine Linux v3.16"
HOME_URL="https://alpinelinux.org/"
BUG_REPORT_URL="https://gitlab.alpinelinux.org/alpine/aports/-/issues"


$ docker run -it alpine ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
45: eth0@if46: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue state UP 
    link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever
```

```
$ docker images alpine
REPOSITORY   TAG       IMAGE ID       CREATED       SIZE
alpine       latest    e66264b98777   6 weeks ago   5.53MB
```

```
$ docker run fedora:27 date
Mon Jul  4 19:33:30 UTC 2022

$ docker run -it fedora:27 date
Mon Jul  4 19:34:00 UTC 2022
```

```
$ docker rm 
agitated_yalow       bold_haibt           determined_wozniak   ecstatic_davinci     jolly_pasteur        quizzical_solomon    
awesome_almeida      brave_kepler         ecstatic_bouman      interesting_shannon  kali                 vigilant_davinci    

$ docker rm agitated_yalow bold_haibt determined_wozniak ecstatic_davinci jolly_pasteur quizzical_solomon awesome_almeida brave_kepler ecstatic_bouman interesting_shannon vigilant_davinci 
agitated_yalow
bold_haibt
determined_wozniak
ecstatic_davinci
jolly_pasteur
quizzical_solomon
awesome_almeida
brave_kepler
ecstatic_bouman
interesting_shannon
vigilant_davinci
```


## Posar nom de container i nom de host

En aquest apartat veurem com assignar nom als containers, tant nom de container com nom de host. El nom del container l’identifica com un objecte de docker (igual que el nom d’una màquina virtual en el menú de les màquines virtuals la identifica com per exemple Debian-test-dns). El nom de host és el nom que té un cop engegat el host que simula el container, el nom DNS, igual que una màquina virtual un cop engegada té un  hostname (per exemple dnsserver).

**Recordeu**: no hi poden haver dos containers amb el mateix nom.

Observem que:

  * **--name nomcontainer** permet assignar el nom que ha de tenir al container. Atenció que no poden haver-hi dos containers amb el matei nom. Observeu en l’exemple que el nom del host és server mentre que el nom del container és myfedora

  * **-h  hostname** és el nom que identifica el host que representa el container. Dins el container el hostname serà aquest valor.

```
$ docker run --name myfedora -h server -it fedora:27  /bin/bash
[root@server /]# cat /etc/hostname
server
[root@server /]# 
```

```
$ docker ps
CONTAINER ID   IMAGE       COMMAND       CREATED          STATUS          PORTS     NAMES
2358aa9cb037   fedora:27   "/bin/bash"   31 seconds ago   Up 30 seconds             myfedora
```

```
[root@server /]# exit
exit
```

  * Prove-ho ara amb un alpine

```
$ docker run --name prova -h host1 -it alpine 
/ # hostname
host1

/ # cat /etc/hostname
host1
```

```
$ docker ps
CONTAINER ID   IMAGE     COMMAND     CREATED          STATUS          PORTS     NAMES
7828c6d57324   alpine    "/bin/sh"   40 seconds ago   Up 40 seconds             prova
```

  * També ab un debian

```
$ docker run --name debian  -h debian -it debian cat /etc/os-release 
PRETTY_NAME="Debian GNU/Linux 11 (bullseye)"
NAME="Debian GNU/Linux"
VERSION_ID="11"
VERSION="11 (bullseye)"
VERSION_CODENAME=bullseye
ID=debian
HOME_URL="https://www.debian.org/"
SUPPORT_URL="https://www.debian.org/support"
BUG_REPORT_URL="https://bugs.debian.org/"
```


**Problema amb noms duplicats**

Aquest és un problema molt usual, en l’exemple que acabem de fer hem generat un container debian anomenat debian que s’ha executat i aturat, però encara existeix, Si ara intentem generar un nou container amb el mateix nom fallarà.

  * El missatge d’error: conflict és molt típic i indica que ja tenim un container amb el mateix nom. 

  * Cal esborra el container o bé el que es crea que tingui un nom diferent.

  * En general el que cal és ser nets i endreçats i no tenir container innecessaris que generin conflictes amb el nom. Un mecanisme és que s’eliminin automàticament en acabar.

```
$ docker ps -a
CONTAINER ID   IMAGE                    COMMAND                 CREATED              STATUS                          PORTS     NAMES
82ad61b4eef5   debian                   "cat /etc/os-release"   About a minute ago   Exited (0) About a minute ago             debian
7828c6d57324   alpine                   "/bin/sh"               4 minutes ago        Up 4 minutes                              prova
2358aa9cb037   fedora:27                "/bin/bash"             7 minutes ago        Exited (0) 5 minutes ago                  myfedora


$ docker run --name debian  -h debian -it debian id
docker: Error response from daemon: Conflict. The container name "/debian" is already in use by container "82ad61b4eef59decea33a038746bf323b0154f835d2a32dd0301c2036eea2420". You have to remove (or rename) that container to be able to reuse that name.
See 'docker run --help'.
```

```
$ docker rm debian 
debian

$ docker run --name debian  -h debian -it debian id
uid=0(root) gid=0(root) groups=0(root)
```

```
$ docker rm debian prova myfedora
debian
Prova
myfedora
```


## Eliminar un container automàticament quan finalitza

En tots els apartats anteriors hem vist que quan anem fent proves amb els containers aquests es van quedant aturats però continuen existent. Si es vol tenir organitzat l’entorn i no malbaratar recursos cal anar-los eliminant, però és motl cansat estar usant l’ordre  docker rm constantment. 

Un mecanisme molt  pràctic en generar containers és fer que s’eliminin automàticamwnt un cop finalitzin amb l’opció --rm. Aquesta opció no l’hem d’utilitzar si el nostre propòsit és que el container perduri i en poguem fer execucions posteriors.

Aquesta opció es pot usar en conjunció amb les altres opcions però si el container l’eliminem en acabar moltes vegades perd el sentit assignar-li un nom al container amb --name (perquè si l’eliminara!).

  * Ho provem amb un debian interactiu

```
$ docker run --rm --name debian -h debian -it debian /bin/bash
root@debian:/# cat /etc/hostname
debian
root@debian:/# exit
exit
```

```
$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```

  * Amb un fedora tipus ordre

```
$ docker run --rm -it fedora:27 date
Mon Jul  4 19:58:56 UTC 2022

$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
$ 
```

  * També amb un alpine

```
$ docker run --rm -h myhost.edt.org -it alpine hostname
myhost.edt.org


$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```


## Containers: start / stop / attach i altres

Com a colofó a la part bàsic de gestió d’imatges i container anem a veure que els containers es poden engegar amb run, aturar amb stop, pausar i reanudar amb pause i unpause i tornar a connectar amb attach.

  * Creem un container debian (sense nom i sense --rm) i sortim. 

  * El container encara existeix, amb docker ps -a podem veure el seu nom.

  * El tornem a engegar amb docker start <container>. Ara està engegat però no hi estem connectats! No estem en una sessió interactica connectats al container.

  * Per fer-ho cal fer  docker attach <container> i passarem a estar dins del container en la sessió interactiva que hi hem iniciat.

```
$ docker run -it debian /bin/bash
root@6b608fd5e511:/# exit
exit

$ docker ps -a
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
6b608fd5e511   debian                   "/bin/bash"   About a minute ago   Exited (0) About a minute ago             lucid_greider

$ docker start lucid_greider 
lucid_greider

$ docker ps 
CONTAINER ID   IMAGE     COMMAND       CREATED         STATUS         PORTS     NAMES
6b608fd5e511   debian    "/bin/bash"   2 minutes ago   Up 2 seconds             lucid_greider

$ docker attach lucid_greider 
root@6b608fd5e511:/# id
uid=0(root) gid=0(root) groups=0(root)
```

  * Ara que el container està engegat des d’una altra sessió podem aturar-lo, pausar-lo, reanudar-lo, etc. Sovint aturar un container amb docker stop <container> tarda una estona.

  * Recordem d’eliminar-lo amb docker rm <container>.

```
$ docker pause lucid_greider 
lucid_greider

$ docker unpause lucid_greider 
lucid_greider

$ docker stop lucid_greider 
lucid_greider
```

```
$ docker rm lucid_greider 
lucid_greider
```

  * Les accions que es poden fer en un container són

```
$ docker container 
attach   cp       diff     export   kill     ls       port     rename   rm       start    stop     unpause  wait     
commit   create   exec     inspect  logs     pause    prune    restart  run      stats    top      update   
```


## Docker exec per  executar ordres dins de containers

Una ordre espectacularment pràctica és docker exec, que permet executar comandes dins de containers i fins i tot obrir més d’una sessió interactiva en el container.

```
docker exec -it <container> ordre
```

  * Engegem un container debian interactiu.

  * Des d’una altra consola executem una ordre desatesa dins del container.

```
$ docker run --rm -it debian /bin/bash
root@a9a395224330:/# date
Mon Jul  4 20:17:27 UTC 2022
root@a9a395224330:/# 
```

```
$ docker exec -it peaceful_leavitt  id
uid=0(root) gid=0(root) groups=0(root)

$ docker exec -it peaceful_leavitt  cat /etc/os-release
PRETTY_NAME="Debian GNU/Linux 11 (bullseye)"
NAME="Debian GNU/Linux"
VERSION_ID="11"
VERSION="11 (bullseye)"
VERSION_CODENAME=bullseye
ID=debian
HOME_URL="https://www.debian.org/"
SUPPORT_URL="https://www.debian.org/support"
BUG_REPORT_URL="https://bugs.debian.org/"
```


  * Podem engegar una segona sessió interactiva dins del containers si volem, en lloc de fer una ordre llancem un shell amb /bin/bash per exemple.

  * Amb l’ordre docker top <container> podem veure els porcessos que s’executen dins d’un container. En aquest cas ara tenim dos shells engegats.

  * El container és viu mentre es manté la sessió interactica inicial viva, el PID 1 és el /bin/bash amb el que s’ha iniciat el container en el docker run. (Atenció: tancar la sessió primera que hem obert!).

```
$ docker exec -it peaceful_leavitt  /bin/bash
root@a9a395224330:/# pwd
/
```

```
$ docker top peaceful_leavitt 
UID                 PID                 PPID                C                   STIME               TTY                 TIME                CMD
root                25207               25185               0                   22:17               pts/0               00:00:00            /bin/bash
root                25594               25185               0                   22:20               pts/1               00:00:00            /bin/bash
```

```
root@a9a395224330:/# exit
exit

$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```

  * Provem-ho ara amb un alpine (el seu shell és el /bin/sh)

  * Des d’una altra sessió executar ordres desateses dins de l’alpine.

  * També hi podem obrir un segon shell on fem ordres sleep i des d’una altra sessió mirem tot el que s’està executant amb docker top <container>.

```
$ docker run --rm --name alpine -it alpine
/ # id
uid=0(root) gid=0(root) groups=0(root),1(bin),2(daemon),3(sys),4(adm),6(disk),10(wheel),11(floppy),20(dialout),26(tape),27(video)

/ # cat /etc/os-release
NAME="Alpine Linux"
ID=alpine
VERSION_ID=3.16.0
PRETTY_NAME="Alpine Linux v3.16"
HOME_URL="https://alpinelinux.org/"
BUG_REPORT_URL="https://gitlab.alpinelinux.org/alpine/aports/-/issues"
```

```
$ docker exec -it alpine hostname
c426b9591747
 
$ docker exec -it alpine id
uid=0(root) gid=0(root) groups=0(root),1(bin),2(daemon),3(sys),4(adm),6(disk),10(wheel),11(floppy),20(dialout),26(tape),27(video)

$ docker exec -it alpine date
Mon Jul  4 20:24:07 UTC 2022
```

```
$ docker exec -it alpine /bin/sh
/ # sleep 12345 &
/ # sleep 12345 &
/ # 
```

```
$ docker top alpine 
UID                 PID                 PPID                C                   STIME               TTY                 TIME                CMD
root                25978               25958               0                   22:23               pts/0               00:00:00            /bin/sh
root                26344               25958               0                   22:25               pts/1               00:00:00            /bin/sh
root                26351               26344               0                   22:25               pts/1               00:00:00            sleep 12345
root                26367               26344               0                   22:26               pts/1               00:00:00            sleep 12345
```

  * Ara podem aturar el container també des de fora. Totes les dues sessions es tancaran automàticament.

```
$ docker stop alpine
alpine
```













