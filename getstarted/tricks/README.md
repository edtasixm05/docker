# getting started - Varis
## @edt ASIX-M05 Curs 2021-2022


### Exemple de Getting Started - 6: Varis 


Exemples i tricks per usar per desplegar ràpidament containers, compose, stack, swarm ,etc.


**Scale**

```
docker swarm init

docker service create --replicas 1 --name helloworld alpine ping docker.com

docker service scale helloworld=3

docker service ls
docker service ps helloworld
docker service rm helloworld
```


**Rolling updates**

```
docker service create --replicas 3 --name redis --update-delay 10s redis:3.0.6

docker service update --image redis:3.0.7 redis

docker service update --rollback redis

docker service ls
docker service ps redis
docker service rm redis
```


**Drain nodes**

```
docker node update --availability drain worker1

docker node update --availability active worker1

docker node update --availability pause worker1

docker node update --availability active worker1
```


**Servei nginx**


```
docker service create --name my-web --publish published=8080,target=80 --replicas 2 nginx

docker service ls
docker service ps my-web
docker service rm my-web
```


**Add Published ports**

```
docker service update \
  --publish-add published=<PUBLISHED-PORT>,target=<CONTAINER-PORT> \
  <SERVICE>

docker service create --name my-web  --replicas 2 nginx

docker service update --publish-add published=8080,target=80 my-web

docker service ls
docker service ps my-web
docker service rm my-web
```

**Servidor DNS Cache**

```
stixes/dns-cache
dockercloud/dnscache

# TCP only
docker service create --name dns-cache --publish published=53,target=53 dns-cache
docker service create --name dns-cache -p 53:53 dns-cache

# UDP Only
docker service create --name dns-cache --publish published=53,target=53,protocol=udp dns-cache
docker service create --name dns-cache -p 53:53/udp dns-cache

# TCP & UDP
docker service create --name dns-cache --publish published=53,target=53 --publish published=53,target=53,protocol=udp dns-cache
docker service create --name dns-cache -p 53:53 -p 53:53/udp dns-cache
```

```
# Verificar respostes (per defecte host usa UDP)
host www.escoladeltreball.org 127.0.0.1 

# Verificar respostes TCP
host -T www.escoladeltreball.org 127.0.0.1

# Verificar respostes UDP
host -U www.escoladeltreball.org 127.0.0.1
```

```
# Publicar TCP i afegir UDP
docker service create --name dns-cache -p 53:53 dns-cache
docker service update --publish-add published=53,target=53,protocol=udp dns-cache
```

**Bypass the Routing Mesh**

```
docker service create --name dns-cache \
  --publish published=53,target=53,protocol=udp,mode=host \
  --mode global \
  stixes/dns-cache
```

Mode ingress routing mesh -->    mode=ingress,  default mode

Mode no routing mesh -->    mode=host

Container replication ---> default mode

Container global (1 for every node) --> --mode=global

```
docker service create --name dns-cache --publish published=53,target=53,protocol=udp,mode=host --mode global stixes/dns-cache
```





