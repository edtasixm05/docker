# getting started - Visites2
## @edt ASIX-M05 Curs 2021-2022


### Exemple de Getting Started - 3: Comptador de visites-2

En aquest exemple es crea una pàgina web amb un comptador de visites (com amb
l'exemple comptador de visites). La diferència és que:

 * Aqui no es genera la imatge 'abans' sinó que en fer el desplegament
   amb *docker-compose* es fa el build de la imatge.

 * Es munta el directori amb el codi python de manera que es pot modificar
   'en calent' la pàgina web.


#### Pas-1 Crera a app i fer el desplegament

```
docker-compose up -d

<browser> http://localhost:5000
wget  http://localhost:5000 
```

```
$ docker-compose ps
           Name                         Command               State                    Ports                  
--------------------------------------------------------------------------------------------------------------
getstartedvisites2_redis_1   docker-entrypoint.sh redis ...   Up      6379/tcp                                
getstartedvisites2_web_1     flask run                        Up      0.0.0.0:5000->5000/tcp,:::5000->5000/tcp

```

```
docker-compose down
docker-compose -d up
```

#### Editar interactivament la web

Tornar a generar un nou docker-compose.yml però ara muntant un directori
del host amfitrió al container, on hi ha el codi python de l'aplicació (/code).
La variable FLASK_ENV pren el valor *development* per tal de que a cada 
canvi del codi es reinicialitzi el servei.

```
$ docker-compose -f docker-compose-pas2.yml up -d
Creating network "visites2_default" with the default driver
Building web
Step 1/9 : FROM python:3.7-alpine
 ---> 7642396105af
Step 2/9 : WORKDIR /code
 ---> Using cache
 ---> 163247482130
Step 3/9 : ENV FLASK_APP app.py
 ---> Using cache
 ---> 521ec6800a94
Step 4/9 : ENV FLASK_RUN_HOST 0.0.0.0
 ---> Using cache
 ---> c338cfdb2741
Step 5/9 : RUN apk add --no-cache gcc musl-dev linux-headers
 ---> Using cache
 ---> abbba6e83160
Step 6/9 : COPY requirements.txt requirements.txt
 ---> Using cache
 ---> b55b8fecfccc
Step 7/9 : RUN pip install -r requirements.txt
 ---> Using cache
 ---> 652276acf431
Step 8/9 : COPY . .
 ---> efbbfba962c0
Step 9/9 : CMD ["flask", "run"]
 ---> Running in 1807ab44a5c6
Removing intermediate container 1807ab44a5c6
 ---> 3606d2ec1da3
Successfully built 3606d2ec1da3
Successfully tagged visites2_web:latest
WARNING: Image for service web was built because it did not already exist. To rebuild this image you must use `docker-compose build` or `docker-compose up --build`.
Creating visites2_redis_1 ... done
Creating visites2_web_1   ... done

$ vim app.py 

$ docker-compose -f docker-compose-pas2.yml down
Stopping visites2_web_1   ... done
Stopping visites2_redis_1 ... done
Removing visites2_web_1   ... done
Removing visites2_redis_1 ... done
Removing network visites2_default
```

