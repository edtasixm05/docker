



Engegar el servei postgres en la xarxa per defecte
```
$ docker run --rm --name training  -e POSTGRES_PASSWORD=passwd -e POSTGRES_DB=training  -d edtasixm05/getstarted:postgres
d57c366ad7b0075579e22555e1b520b06870da3756c8ca23f91b22f70caf003b
```

Engegar un client psql interactu que comunica amb el servei postgres anterior, és una imatge
ja preparada *edtasixm05/postgres21:client-psql-py* amb python i el client psql de postgres.
(el password de postgres és *passwd*)
Munta el directori sql-injection amb els exemples de python a provar al directori /tmp
per poder practicar.

```
$ docker run  --rm -v $(pwd):/tmp -it  edtasixm05/postgres21:client-psql-py
/ # python /tmp/12-popen-sql.py 
Password for user postgres: 
2111,JCP Inc.,103,50000.00
2102,First Corp.,101,65000.00
2103,Acme Mfg.,105,50000.00
2123,Carter & Sons,102,40000.00
2107,Ace International,110,35000.00
2115,Smithson Corp.,101,20000.00
2101,Jones Mfg.,106,65000.00
2112,Zetacorp,108,50000.00
2121,QMA Assoc.,103,45000.00
2114,Orion Corp,102,20000.00
2124,Peter Brothers,107,40000.00
2108,Holm & Landis,109,55000.00
2117,J.P. Sinclair,106,35000.00
2122,Three-Way Lines,105,30000.00
2120,Rico Enterprises,102,50000.00
2106,Fred Lewis Corp.,102,65000.00
2119,Solomon Inc.,109,25000.00
2118,Midwest Systems,108,60000.00
2113,Ian & Schmidt,104,20000.00
2109,Chen Associates,103,25000.00
2105,AAA Investments,101,45000.00
```

```
/ # python /tmp/13-popen-sql-injectat.py  "select * from clientes;"
Password for user postgres: 
2111,JCP Inc.,103,50000.00
2102,First Corp.,101,65000.00
2103,Acme Mfg.,105,50000.00
2123,Carter & Sons,102,40000.00
2107,Ace International,110,35000.00
2115,Smithson Corp.,101,20000.00
2101,Jones Mfg.,106,65000.00
2112,Zetacorp,108,50000.00
2121,QMA Assoc.,103,45000.00
2114,Orion Corp,102,20000.00
2124,Peter Brothers,107,40000.00
2108,Holm & Landis,109,55000.00
2117,J.P. Sinclair,106,35000.00
2122,Three-Way Lines,105,30000.00
2120,Rico Enterprises,102,50000.00
2106,Fred Lewis Corp.,102,65000.00
2119,Solomon Inc.,109,25000.00
2118,Midwest Systems,108,60000.00
2113,Ian & Schmidt,104,20000.00
2109,Chen Associates,103,25000.00
2105,AAA Investments,101,45000.00 
```

```
/ # python /tmp/13-sql-injectat.py  "select * from oficinas;"
Password for user postgres: 
22,Denver,Oeste,108,300000.00,186042.00
11,New York,Este,106,575000.00,692637.00
12,Chicago,Este,104,800000.00,735042.00
13,Atlanta,Este,105,350000.00,367911.00
21,Los Angeles,Oeste,108,725000.00,835915.00
```

```
/ # python /tmp/14-popen-sql-multi.py -c 2103 -c 2107
Password for user postgres: 
2103,Acme Mfg.,105,50000.00
2107,Ace International,110,35000.00

```

### Exemples 'nocius' de SQL Injectat


**Exemple amb programa *13-sql-injectat.py***
```
/ # python /tmp/13-sql-injectat.py "select * from oficinas; drop table oficinas;"
Password for user postgres:


/ # python /tmp/13-sql-injectat.py  "select * from oficinas;"
Password for user postgres: 
ERROR:  relation "oficinas" does not exist
LINE 1: select * from oficinas;
```

**Exemple amb programa *14-popen-sql-multi.py***

```
/ # python /tmp/14-popen-sql-multi.py -c 2103 -c "2107;drop table pedidos;"
Password for user postgres: 
2103,Acme Mfg.,105,50000.00
2107,Ace International,110,35000.00
/ # python /tmp/13-sql-injectat.py  "select * from pedidos;"
Password for user postgres: 
ERROR:  relation "pedidos" does not exist
LINE 1: select * from pedidos;
```

```
/ # python /tmp/14-popen-sql-multi.py -c 2103 -c "2107;drop database training;"
Password for user postgres: 
2103,Acme Mfg.,105,50000.00
2107,Ace International,110,35000.00
##nota: no elimina la base de dades
``` 


