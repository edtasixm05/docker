# Swarm
## @edt ASIX-M05 Curs 2021-2022

## Swarm / Nodes


Es poden desplegar aplicacions docker amb docker stack en un o més nodes que formen
un clúster que docker anomena swarm. Per defecte en els swarms es genera una xarxa
*ingress  routing mesh* que permet rèpliques de containers escoltant en un mateix port
i en diferents nodes. D'aquesta manera es poden desplegar aplicacions amb components
distribuïts entre diferents nodes.

Docker swarm genera per defecte una estructura de xarxa que permet a múltiples rèliques 
per exemple d'un container web del port 80 escoltar en el port 80 del host amfitrió o 
de cada un dels hosts (nodes) del swarm, estigui executant-s'hi el container o no.

El model de treball és:

 * Amb **docker swarm** es pot crear un swarm, afegir i eliminar nodes d'un swarm.

 * Amb **docker nodes** es pot gestionar l'estat d'un node, pausar-lo, etc.

 * Amb **docker stack** es poden desplegar aplicacions en un swarm.

 * Amb **docker service** o les configuracions dels serveis dins de **docker-compose**
   es poden definir directives de col·locació dels serveis en els nodes.


### Docker swarm

Tal i com s'ha vist es poden posar un o més nodes en un swarm per crear
un cluster de hosts que treballen onjutament. A l'inici de tot
s'ha vost com crear un swarn en el node *leader*. La mateixa instrucció
indica què cal fer en els nodes *worker* per unir-se al swarm.

Per gestionar noes i swarms convé tractar les ordres:
 
 * docker swarm

 * docker node

```
$ docker swarm 
ca          init        join        join-token  leave       unlock      unlock-key  update      
```

```
$ docker node 
demote   inspect  ls       promote  ps       rm       update   
```

```
$ docker swarm init
Swarm initialized: current node (wgrewlz8t4yx0tznc9ru0dq23) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-5zhhg5u5xf1yvihqwsrq4jd67smmptakyr9dlbhl03g0nefv9j-5lklxx77v427haraii4t8iruw 192.168.1.45:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```

Apagar el swarm iniciat en aquest exercici
```
$ docker swarm leave --force
Node left the swarm.
```

### Docker node

**under construction**

Estat del node
```
$ docker node
demote   inspect  ls   	promote  ps   	rm   	update   

docker node ls
docker node update --availability pause node1
docker node update --availability drain node1
docker node update --availability active node1
```


## Colocació de recursos als nodes: Labels & Placement &  Constraints

Posar etiquetes als nodes
```
docker node update --label-add lloc=local node1
docker node inspect node1
```

Indicar la colocació als services
```
  placement:
    constraints: [node.labels.lloc == local ]

  placement:
    constraints: [node.role == manager]
```

```
$ docker service create \
  --name redis_2 \
  --constraint node.platform.os==linux \
  --constraint node.labels.type==queue \
  redis:3.0.6

$ docker service create \
  --name web \
  --constraint node.labels.region==east \
  nginx:alpine

$ docker service create \
  --replicas 9 \
  --name redis_2 \
  --placement-pref spread=node.labels.datacenter \
  redis:3.0.6

$ docker service create \
  --replicas 9 \
  --name redis_2 \
  --placement-pref 'spread=node.labels.datacenter' \
  --placement-pref 'spread=node.labels.rack' \
  redis:3.0.6

$ docker service create --reserve-memory=4GB --name=too-big nginx:alpine

$ docker service create \
  --name nginx \
  --replicas 2 \
  --replicas-max-per-node 1 \
  --placement-pref 'spread=node.labels.datacenter' \
  nginx

$ docker service create \
  --replicas 3 \
  --network my-network \
  --name my-web \
  nginx
```



