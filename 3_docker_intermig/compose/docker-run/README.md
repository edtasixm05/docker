# docker
## @edt ASIX-M05 Curs 2021-2022

# Compose Vs Docker Run

Llistat d'exemples per converit ordres docker run a fitxers YAML de docker-compose.

Exemples:

 * **web21**
 En aquest exemple anem a veure com ho fariem per desplegar amb docker run el servei web21 que hem creat, en una xarxa pròpia i publicant el port 80. Un cop tinguem l’ordre docker run passarem les opcions al fitxer compose.yml.

 * **ssh21**
 En aquest exemple anem a veure com ho fariem per desplegar amb docker run el servei ssh21 que hem creat, en una xarxa pròpia i publicant el port 22. Un cop tinguem l’ordre docker run passarem les opcions al fitxer compose.yml.

 * **net21**
 En aquest exemple anem a veure com ho fariem per desplegar amb docker run el servei net21 que hem creat, en una xarxa pròpia i publicant el port 7, 13, 19. Un cop tinguem l’ordre docker run passarem les opcions al fitxer compose.yml.

 * **portainer**
 En aquest exemple anem a veure com ho fariem per desplegar amb docker run el servei portainer oficial, en una xarxa pròpia i publicant el port 9000. També cal tenir present que ha de muntar amb bind mount el  socket de Docker que està en la ruta */var/run/docker.sock*. Un cop tinguem l’ordre docker run passarem les opcions al fitxer compose.yml.

 * **visualizer**
 En aquest exemple anem a veure com ho fariem per desplegar amb docker run el servei visualizer oficial, en una xarxa pròpia i publicant el port 8080. També cal tenir present que ha de muntar amb bind mount el  socket de Docker que està en la ruta /var/run/docker.sock. Un cop tinguem l’ordre docker run passarem les opcions al fitxer compose.yml.

 * **ldap21**
 En aquest exemple anem a veure com ho fariem per desplegar amb docker run el servei ldap21, en una xarxa pròpia i publicant el port 389. Un cop tinguem l’ordre docker run passarem les opcions al fitxer compose.yml.

 * **postgres**
 En aquest exemple anem a veure com ho fariem per desplegar amb docker run el servei postgres amb la base de dades training, en una xarxa pròpia i publicant el port 5432. Un cop tinguem l’ordre docker run passarem les opcions al fitxer compose.yml.

 * **ldap+phpldapadmin**
 En aquest exemple s’engega un servidor ldap i el servei phpldapadmin que en permet l’administració gràfica.
 Consulteu l’apartat [Exemple ldap + phpldapadmin](https://docs.google.com/document/d/1mDmvckz6T8_mAfPdgOgZ-GulGBB5KbUGM0qwCjMSUxA/edit#heading=h.16wxqp9aq9o4) del capítol docker compose exemples.

 * **training+adminer**
 En aquest exemple s’engega un servidor postgres amb la base de dades training i l’eina gràfica adminer que en permet l’administració. 

 * **Ldap amb volums de configuració i dades**
 En aquest exemple s’engega un servidor  LDAP amb persistencia de dades tant d ela configuració dnàmica de LDAP com  de les dades de la base de dades dc=edt,dc=org.

 * **postgres oficial amb volum de inicialització i dades**
 En aquest exemple anem a crear un fitxer YML per desplegar un postgres oficial que s’inicialitzi amb les dades de la base de dades training i tingui persintència de dades en un volum. En el fitxer de compose.yml també hi desplegarem l’eina gràfica dminer per poder administrar postgres gràficament.



