# compose:examples/v2
## @edt ASIX-M05 Curs 2021-2022


### Treballar amb docker compose


```
docker-compose up

docker-compose ps

docker-compose top

docker-compose down
```


```
$ docker-compose -f docker-compose-v3-web21-net21-portainer.yml up -d

$ docker-compose -f docker-compose-v3-web21-net21-portainer.yml ps

$ docker-compose -f docker-compose-v3-web21-net21-portainer.yml top

$ docker-compose -f docker-compose-v3-web21-net21-portainer.yml down

```




